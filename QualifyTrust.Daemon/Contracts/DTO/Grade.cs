﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QualifyTrust.Daemon.Contracts
{
	public class Grade : IGrade
	{
		#region Properties
		public Guid Id { get; set; }
		public string Evaluation { get; set; }
		public Guid StudentId { get; set; }
		public Guid TeacherId { get; set; }
		public string TeacherFullName { get; set; }
		public string SubjectId { get; set; }
		public string SubjectName { get; set; }
		public int? GradeValue { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime ModifiedOn { get; set; }
		public bool IsSigned { get; set; }
		public DateTime? SignedOn { get; set; }
		public bool IsLocked { get; set; }
		public DateTime? LockedOn { get; set; }
		public string IsValid { get; set; }
		public string TxHash { get; set; } 

		#endregion

		#region Static Factory
		public static Grade CreateFrom(IGrade grade)
		{
			return new Grade()
			{
				Id = grade.Id,
				Evaluation = grade.Evaluation,
				StudentId = grade.StudentId,
				TeacherId = grade.TeacherId,
				SubjectId = grade.SubjectId, 
				GradeValue = grade.GradeValue,
				CreatedOn = DateTime.UtcNow
			};
		}
		#endregion
	}
}
