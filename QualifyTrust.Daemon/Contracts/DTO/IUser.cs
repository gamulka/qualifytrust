﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;

namespace QualifyTrust.Daemon.Contracts
{
	public interface IUser
	{
		Guid Id { get; }
		string FirstName { get;}
		string LastName { get;}
	}
}
