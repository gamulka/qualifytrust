﻿using Nethereum.ABI.FunctionEncoding.Attributes;

namespace QualifyTrust.Daemon.Contracts
{
	[FunctionOutput]
	public class EthereumGrade
	{
		[Parameter("string", "subjectId", 1)]
		public string Id { get; set; }

		[Parameter("int256", "value", 2)]
		public int Value { get; set; }
	}
}
