﻿using System;

namespace QualifyTrust.Daemon.Contracts
{
	public class User: IUser
	{
		#region Properties
		public virtual Guid Id { get; set; }
		public virtual string FirstName { get; protected set; }
		public virtual string LastName { get; protected set; }

		public string FullName => string.Format("{0} {1}", FirstName, LastName);
		#endregion

		#region Static Factory
		public static User CreateFrom(IUser user)
		{
			return new User()
			{
				Id = user.Id,
				FirstName = user.FirstName,
				LastName = user.LastName
			};
		}
		#endregion
	}
}
