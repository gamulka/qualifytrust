﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QualifyTrust.Daemon.Contracts
{
	public interface IGrade
	{
		Guid Id { get; }
		string Evaluation { get; }
		Guid StudentId { get; }
		Guid TeacherId { get; }
		string TeacherFullName { get; set; }
		string SubjectId { get; }
		string SubjectName { get; set; }
		int? GradeValue { get; }
		DateTime CreatedOn { get; }
		DateTime ModifiedOn { get; }
		bool IsSigned { get; set; }
		bool IsLocked { get; set; }
		DateTime? SignedOn { get; }
		DateTime? LockedOn { get; }
	}
}
