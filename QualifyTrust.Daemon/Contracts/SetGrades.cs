﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace QualifyTrust.Daemon.Contracts
{
	public class GradeValue
	{
		public Guid SubjectId { get; set; }
		public int Grade { get; set; }
	}

	public class SetGrades : Filter
	{
		public GradeValue[] Grades { get; set; }
	}
}
