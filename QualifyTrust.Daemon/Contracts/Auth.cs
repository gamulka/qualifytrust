﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace QualifyTrust.Daemon.Contracts
{
	public class Auth
	{
		public string Email { get; set; }
		public string Password { get; set; }
	}
}