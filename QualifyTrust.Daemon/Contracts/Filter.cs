﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace QualifyTrust.Daemon.Contracts
{
	public class Filter
	{
		public Guid? EvaluationId { get; set; }
		public Guid? DirectorId { get; set; }
		public Guid? TeacherId { get; set; }
		public Guid? StudentId { get; set; }
		public bool Validate { get; set; }
		public uint State { get; set; }
	}
}