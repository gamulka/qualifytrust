﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nethereum.Contracts;
using HermaFx.DesignPatterns;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Hex.HexTypes;

namespace QualifyTrust.Daemon.Ethereum
{
	public interface IEthereumService: IService
	{
		Task<TransactionReceipt> ReleaseContractAsync();
		Task<Contract> GetContractAsync(string contractAddress, string fromAddress = null);
		Task<HexBigInteger> GetTransactionCountAsync(string contractAddress);
	}
}
