﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualifyTrust.Daemon.EthereumService
{
	public class Abi
	{
		public bool constant { get; set; }
		public List<object> inputs { get; set; }
		public string name { get; set; }
		public List<object> outputs { get; set; }
		public bool payable { get; set; }
		public string stateMutability { get; set; }
		public string type { get; set; }
	}

	public class ContractInfo
	{
		public string contractName { get; set; }
		public List<Abi> abi { get; set; }
		public string bytecode { get; set; }
		public string abiString { get; set; }
	}

	public class Bytecode {
		public string @object { get; set; }
	}
}
