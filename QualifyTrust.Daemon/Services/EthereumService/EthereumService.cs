﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using Nethereum.Web3;
using Nethereum.HdWallet;
using QualifyTrust.Daemon.EthereumService;

namespace QualifyTrust.Daemon.Ethereum
{
	public class EthereumService : IEthereumService
	{
		private static readonly global::Common.Logging.ILog _Log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private static ISettings _settings;

		private Web3 _web3;
		private string _abi;
		private string _byteCode;

		public EthereumService(ISettings settings)
		{
			_settings = settings;

			_web3 = GetWeb3Client();
			SetAbiAndBytecode();
		}

		private Web3 GetWeb3Client(string address = null) {

			var serverUrl = _settings.Ethereum.UseModePro
					? _settings.Ethereum.ServerUrlPro
					: _settings.Ethereum.ServerUrlDev;

			var wordList = _settings.Ethereum.Mnemonic;
			var accounts = new Wallet(wordList, null).GetAccount(address ?? _settings.Ethereum.AccountAddress);
			return new Web3(accounts, serverUrl);
		}

		private void SetAbiAndBytecode()
		{
			if (_settings.Ethereum.DeployFromJSON)
			{
				var file = File.ReadAllText(_settings.Ethereum.ContractJSONPath);
				var result = JsonConvert.DeserializeObject<ContractInfo>(file);

				_abi = JsonConvert.SerializeObject(result.abi);
				_byteCode = result.bytecode;
			}
			else {
				var file = File.ReadAllText(_settings.Ethereum.ContractBytecodePath);
				var result = JsonConvert.DeserializeObject<Bytecode>(file);

				_abi = File.ReadAllText(_settings.Ethereum.ContractABIPath);
				_byteCode = result.@object;
			}
		}

		public async Task<TransactionReceipt> ReleaseContractAsync()
		{
			try
			{
				SetAbiAndBytecode(); // << refresh contract data

				var transactionHash = await _web3.Eth.DeployContract.SendRequestAndWaitForReceiptAsync(
					abi: _abi,
					contractByteCode: _byteCode,
					from: _settings.Ethereum.AccountAddress,
					gas: new HexBigInteger(_settings.Ethereum.DefaultGas)
				);

				return transactionHash;
			}
			catch (Exception ex)
			{
				_Log.ErrorFormat("Error releasing contract, {0}", ex.Message, ex);
				return null;
			}
		}

		public async Task<Contract> GetContractAsync(string contractAddress, string fromAddress = null)
		{
			_web3 = GetWeb3Client(fromAddress);
			return await Task.FromResult(_web3.Eth.GetContract(_abi, contractAddress));
		}

		public async Task<HexBigInteger> GetTransactionCountAsync(string contractAddress) 
		{
			return await _web3.Eth.Transactions.GetTransactionCount.SendRequestAsync(contractAddress);
		}
	}
}
