﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualifyTrust.Daemon.Ethereum
{
	public enum Services
	{
		SetDirector,
		SetGrade,
		SetLockState,
		SetStudentData,
		SetTeacher,
		SignEvaluation,
		GetTotalGrades,
		StudentData,
		Grades
	}
}
