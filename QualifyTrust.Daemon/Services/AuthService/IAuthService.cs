﻿using HermaFx.DesignPatterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace QualifyTrust.Daemon.AuthService
{
	public interface IAuthService : IService
	{
		Task<ClaimsPrincipal> GetUserFromAccessTokenAsync(string accessToken);
		Task<string> ValidateUserAsync(string email, string password);
	}
}
