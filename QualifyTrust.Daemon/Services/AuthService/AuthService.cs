﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Collections.Concurrent;
using Microsoft.IdentityModel.Tokens;
using QualifyTrust.Daemon.Persistence;

namespace QualifyTrust.Daemon.AuthService
{
	public class AuthService : IAuthService
	{
		#region Private
		private static readonly global::Common.Logging.ILog _log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly ConcurrentDictionary<string, User> _activeAccessToken = new ConcurrentDictionary<string, User>();
		private static IDbProvider _dbProvider;
		private static ISettings _settings;
		#endregion

		public AuthService(ISettings settings, IDbProvider dbProvider)
		{
			_settings = settings;
			_dbProvider = dbProvider;
		}

		private static string GetSmartContractAddressFor(User user)
		{
			using (var db = _dbProvider.GetRepository())
			{
				var smartContractInfo = db.FetchWhere<SmartContractInfo>(x => x.UserId == user.Id).FirstOrDefault();

				return smartContractInfo != null
					? smartContractInfo.Address
					: string.Empty;
			}
		}

		private static string GenerateTokenJwt(User user)
		{
			var secretKey = _settings.Auth.SecretKey;
			var audienceToken = _settings.Auth.AudienceToken;
			var issuerToken = _settings.Auth.IssuerToken;
			var expireTime = _settings.Auth.ExpireMinutes;

			var securityKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(secretKey));
			var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
			var smartContractAddress = GetSmartContractAddressFor(user);

			// create a claimsIdentity
			ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[] {
				new Claim(ClaimTypes.Name, user.FirstName.Trim()),
				new Claim(ClaimTypes.Surname, user.LastName.Trim()),
				new Claim(ClaimTypes.Email, user.UserName),
				new Claim(ClaimTypes.Role, ((int)user.Role).ToString()),
				new Claim("picture", user.Photo),
				new Claim("id", user.Id.ToString()),
				new Claim("ethereumAddress", user.Role == UserRole.Student ? smartContractAddress : user.EthereumAccountAddress)
			});

			// create token to the user
			var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
			var jwtSecurityToken = tokenHandler.CreateJwtSecurityToken(
				audience: audienceToken,
				issuer: issuerToken,
				subject: claimsIdentity,
				notBefore: DateTime.UtcNow,
				expires: DateTime.UtcNow.AddMinutes(Convert.ToInt32(expireTime)),
				signingCredentials: signingCredentials);

			var jwtTokenString = tokenHandler.WriteToken(jwtSecurityToken);
			return jwtTokenString;
		}

		public Task<ClaimsPrincipal> GetUserFromAccessTokenAsync(string accessToken)
		{
			var activeToken = _activeAccessToken.ContainsKey(accessToken);
			if (!activeToken)
			{
				return null;
			}

			var handler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
			var token = handler.ReadToken(accessToken) as SecurityToken;
			var secretKey = _settings.Auth.SecretKey;
			var securityKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(secretKey));

			var validations = new TokenValidationParameters
			{
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = securityKey,
				ValidateIssuer = false,
				ValidateAudience = false
			};

			// TODO: uas expire date to send 401 request too
			// token.ValidTo

			var claims = handler.ValidateToken(accessToken, validations, out token);

			return Task.FromResult(claims);
		}

		public Task<string>  ValidateUserAsync(string email, string password)
		{
			_log.DebugFormat("Validating user {0}", email);

			using (var db = _dbProvider.GetRepository())
			{
				var user = db.FetchWhere<User>(x => x.UserName == email && x.Password == password).FirstOrDefault();
				if (user == null)
				{
					return Task.FromResult(string.Empty);
				}

				var token = GenerateTokenJwt(user);

				_activeAccessToken.TryAdd(token, user);

				return Task.FromResult(token);
			}
		}
	}
}
