﻿using System;
using System.Reflection;

using Common.Logging;
using Topshelf;
using Nancy.Hosting.Self;
using QualifyTrust.Daemon.Persistence;
using System.Net;

namespace QualifyTrust.Daemon
{
	public class Service : ServiceControl
	{
		private string ServiceName => "QualifyTrust.Daemon";

		private readonly ILog _Log;
		private readonly NancyHost _host;
		private readonly ISettings _settings;
		private readonly IDbProvider _dbProvider;

		public Service(ILog logger,
			ISettings settings,
			NancyHost host,
			IDbProvider dBProvider)
		{
			_Log = logger ?? new Common.Logging.Simple.NoOpLogger();
			_settings = settings;
			_host = host;
			_dbProvider = dBProvider;
		}

		#region ServiceControl Members

		public bool Start(HostControl hostControl)
		{
			_Log.Info($"{ServiceName} service starting...");

			try
			{
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
				_host.Start();
			}
			catch (TargetInvocationException e)
			{
				_Log.Error("Unable to start ServiceHost!", e);
				return false;
			}
			catch (Exception e)
			{
				_Log.Error("Unable to start ServiceHost!", e);
				return false;
			}
			_Log.Info($"{ServiceName} service started");
			return true;
		}

		public bool Stop(HostControl hostControl)
		{
			_Log.Info($"{ServiceName} service stopping...");

			try
			{
				_host?.Stop();
			}
			catch (Exception ex)
			{
				_Log.Warn("Unexpected error while stopping Nancy Host!", ex);
			}

			return true;
		}

		#endregion
	}
}
