﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HermaFx;
using QualifyTrust.Daemon.Persistence;
using QualifyTrust.Daemon.Ethereum;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;

namespace QualifyTrust.Daemon.Api
{
	public interface IEthereumController
	{
		Task DeployContractsAsync();
	}

	public class BootController : IEthereumController
	{
		private static readonly global::Common.Logging.ILog _Log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private static IDbProvider _dbProvider;
		private static ISettings _settings;
		private static IEthereumService _ethereumService;
		private static string _owner;
		private static HexBigInteger _gas;

		public BootController(
			ISettings settings,
			IDbProvider dbProvider,
			IEthereumService ethereumService)
		{
			_settings = settings;
			_dbProvider = dbProvider;
			_ethereumService = ethereumService;

			_owner = _settings.Ethereum.AccountAddress;
			_gas = new HexBigInteger(settings.Ethereum.DefaultGas);
		}

		#region private methods
		private IEnumerable<User> GetUser(UserRole role)
		{
			using (var db = _dbProvider.GetRepository())
			{
				return db.FetchWhere<User>(x => x.Role == role);
			}
		}

		private IEnumerable<User> GetTeachersFor(Guid studentId)
		{
			var teachers = new List<User>();

			using (var db = _dbProvider.GetRepository())
			{
				db.FetchWhere<Grade>(x => x.StudentId == studentId).Select(x => x.TeacherId).Distinct()
					.ForEach(x =>
					{
						var teacher = db.FetchWhere<User>(_ => _.Id == x).Single();
						teachers.Add(teacher);
					});
			}
			return teachers;
		}

		private HexBigInteger SetDirector(IEnumerable<SmartContractInfo> contractsInfo, HexBigInteger nonce = null)
		{
			Guard.IsNotNullNorDefault(contractsInfo, nameof(contractsInfo));

			var currentNonce = nonce == null
				? _ethereumService.GetTransactionCountAsync(_owner).Result
				: nonce;

			var index = 0;
			var directors = GetUser(UserRole.Director);
			foreach (var contracInfo in contractsInfo)
			{
				var contract = _ethereumService.GetContractAsync(contracInfo.Address).Result;

				directors.ForEach(director =>
				{
					var address = director.EthereumAccountAddress;

					_Log.DebugFormat("Settings director: {0} address for studenId: {1}", address, contracInfo.UserId);

					var transaction = new TransactionInput()
					{
						From = _owner,
						Gas = _gas,
						Nonce = new HexBigInteger(currentNonce.Value + index++)
					};
					var txHash = contract.GetFunction(Services.SetDirector.ToString())
						.SendTransactionAsync(transaction, address)
						.Result;
				});
			};

			return new HexBigInteger(currentNonce.Value + index);
		}

		private HexBigInteger SetTeacher(IEnumerable<SmartContractInfo> contractsInfo, HexBigInteger nonce = null)
		{
			Guard.IsNotNullNorDefault(contractsInfo, nameof(contractsInfo));

			var currentNonce = nonce == null
				? _ethereumService.GetTransactionCountAsync(_owner).Result
				: nonce;

			var index = 0;
			foreach (var contracInfo in contractsInfo)
			{
				var contract = _ethereumService.GetContractAsync(contracInfo.Address).Result;
				var studentId = contracInfo.UserId;
				var teachers = GetTeachersFor(studentId);

				teachers.ForEach(teacher =>
				{
					var address = teacher.EthereumAccountAddress;

					_Log.DebugFormat("Settings teacher: {0} address for studenId: {1}", address, studentId);
					var transaction = new TransactionInput()
					{
						From = _owner,
						Gas = _gas,
						Nonce = new HexBigInteger(currentNonce.Value + index++)
					};

					var txHash = contract.GetFunction(Services.SetTeacher.ToString())
						.SendTransactionAsync(transaction, address)
						.Result;
				});
			};

			return new HexBigInteger(currentNonce.Value + index);
		}

		private HexBigInteger SetStudentsData(IEnumerable<SmartContractInfo> contracts, HexBigInteger nonce = null)
		{
			Guard.IsNotNullNorDefault(contracts, nameof(contracts));

			var currentNonce = nonce == null
				? _ethereumService.GetTransactionCountAsync(_owner).Result
				: nonce;

			var index = 0;
			foreach (var contrac in contracts)
			{
				var address = contrac.Address;
				var studentId = contrac.UserId;

				_Log.DebugFormat("Settings student data for studenId: {0}", studentId);

				var student = GetUser(UserRole.Student).Single(x => x.Id == studentId);
				var contract = _ethereumService.GetContractAsync(address).Result;
				var transaction = new TransactionInput()
				{
					From = _owner,
					Gas = _gas,
					Nonce = new HexBigInteger(currentNonce.Value + index++)
				};
				var txHash = contract.GetFunction(Services.SetStudentData.ToString())
					.SendTransactionAsync(transaction,
						address,
						student.Id.ToString(),
						student.FullName
					).Result;
			};

			return new HexBigInteger(currentNonce.Value + index);
		}
		#endregion

		#region Release contracts

		private IEnumerable<SmartContractInfo> ReleaseContracts(Dictionary<string, IEnumerable<IEntity>> studentContracts)
		{
			Guard.IsNotNullNorDefault(studentContracts, nameof(studentContracts));

			var logMessage = "Deploying contracts for";
			var relesedsContracts = new List<SmartContractInfo>();

			studentContracts.ForEach(contract =>
			{
				// Deploy contracts in Ethereum
				foreach (var student in contract.Value)
				{
					_Log.Debug($"{logMessage} studentId: {student.Id}...");

					var receipt = _ethereumService.ReleaseContractAsync().Result;
					if (receipt == null)
					{
						_Log.WarnFormat("Unable to deploy contract: Hash: {0}, Address: {1} (Discarted)", receipt.TransactionHash, receipt.ContractAddress);
						continue;
					}

					_Log.Debug($"{logMessage} contractAddress: {receipt.ContractAddress} [DONE]");

					relesedsContracts.Add(SmartContractInfo.Create(student.Id, receipt.TransactionHash, receipt.ContractAddress));
				}
			});

			return relesedsContracts;
		}

		public Task DeployContractsAsync()
		{
			var contractPath = _settings.Ethereum.ContractJSONPath;

			// Persist relased contracts
			var db = _dbProvider.GetRepository();
			using (var tx = db.GetTransaction())
			{
				db.TruncateTable(new SmartContractInfo());
				tx.Complete();
			}

			// Gets all contract owners
			var studentsContracts = new Dictionary<string, IEnumerable<IEntity>>();
			studentsContracts.Add(contractPath, GetUser(UserRole.Student).OrderBy(o => o.FullName));

			try
			{
				// Deploy all contracts
				var releasedContracts = ReleaseContracts(studentsContracts);

				var nonce = SetDirector(releasedContracts);
				nonce = SetTeacher(releasedContracts, nonce);
				SetStudentsData(releasedContracts, nonce);

				// Persist relased contracts
				using (var tx = db.GetTransaction())
				{
					db.InsertBulk(releasedContracts);
					tx.Complete();
				}
			}
			catch (Exception ex) 
			{
				_Log.ErrorFormat("Error while deploy SmartContracts {0}", ex);
			}

			return Task.FromResult(0);
		}

		#endregion
	}
}
