﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nancy;
using Nancy.Swagger.Annotations.Attributes;
using Swagger.ObjectModel;

namespace QualifyTrust.Daemon.Api
{
	public class BootModule : NancyModule
	{

		#region Private
		private static readonly global::Common.Logging.ILog _log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private static IEthereumController _ethereumController;
		#endregion

		#region .ctor
		public BootModule(BootController ethereumController) : base($"/ethereum")
		{
			_ethereumController = ethereumController;

			Post("/contracts/deploy", _ => DeployAsync());
		}
		#endregion

		[Route(HttpMethod.Post, "/contracts/deploy")]
		[Route(Tags = new[] { "Boot" }, Summary = "Deploy all users contracts in Ethereum")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(HttpStatusCode))]
		private async Task<HttpStatusCode> DeployAsync()
		{
			_log.Debug($"processing request: {this.Request.Url}");

			await _ethereumController.DeployContractsAsync();

			return HttpStatusCode.OK;
		}
	}
}
