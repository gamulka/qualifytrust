﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Swagger.Annotations.Attributes;
using Swagger.ObjectModel;

using QualifyTrust.Daemon.AuthService;
using QualifyTrust.Daemon.Contracts;

namespace QualifyTrust.Daemon.Api
{
	public class AuthModule : NancyModule
	{
		#region Private
		private static readonly global::Common.Logging.ILog _log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private static IAuthService _authService;
		#endregion

		#region .ctor
		public AuthModule(IAuthService authService) : base("/")
		{
			_authService = authService;

			Post("auth/login", _ => GetTokenAsync(this.Bind<Auth>()));
		}
		#endregion

		[Route(HttpMethod.Post, "auth/login")]
		[Route(Tags = new[] { "auth" }, Summary = "Get authorization token")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(string))]
		private async Task<dynamic> GetTokenAsync([RouteParam(ParameterIn.Body)] Auth request)
		{
			_log.Debug($"processing request: {this.Request.Url}");

			var token = await _authService.ValidateUserAsync(request.Email, request.Password);

			return string.IsNullOrEmpty(token)
				? new Nancy.Response { StatusCode = HttpStatusCode.Unauthorized }
				: this.Response.AsJson(new { access_token = token });
		}
	}
}
