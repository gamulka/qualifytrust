﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using QualifyTrust.Daemon.Persistence;
using QualifyTrust.Daemon.Contracts;
using Nancy;
using HermaFx;
using QualifyTrust.Daemon.Ethereum;

namespace QualifyTrust.Daemon.Api
{
	public interface IGradeController
	{
		Task<IEnumerable<IGrade>> GetGradesAsync(Filter request);
		Task<Dictionary<Guid, object>> SetGradesAsync(SetGrades request);
		Task<IEnumerable<IGrade>> GetEvaluationGradesAsync(Guid evaluationId);
		Task<IEnumerable<Guid>> GetEvaluationLockedAsync(Guid teacherId);
	}

	public class GradeController : IGradeController
	{
		private static readonly global::Common.Logging.ILog _Log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		IDbProvider _dbProvider;
		private static IEthereumService _ethereumService;

		public GradeController(IDbProvider dbProvider, IEthereumService ethereumService)
		{
			_dbProvider = dbProvider;
			_ethereumService = ethereumService;
		}

		private IEnumerable<T> GetItems<T>()
		{
			using (var db = _dbProvider.GetRepository())
			{
				return db.Fetch<T>();
			}
		}

		private Task<IEnumerable<EthereumGrade>> GetGradesFromBlockchainAsync(Filter request)
		{
			Guard.IsNotNullNorDefault(request, nameof(request));
			Guard.IsNotNullNorDefault(request.StudentId, nameof(request.StudentId));

			_Log.DebugFormat("Getting subjects of studenId: {0}", request.StudentId);

			var results = new List<EthereumGrade>();

			var studentId = request.StudentId.Value;
			var evaluationId = request.EvaluationId.Value.ToString("n");
			var contracts = GetItems<SmartContractInfo>();
			var address = contracts.Single(x=> x.UserId == studentId).Address;

			// Connect to Ethereum...
			var contract = _ethereumService.GetContractAsync(address).Result;
			var totalGrades = contract.GetFunction(Services.GetTotalGrades.ToString()).CallAsync<uint>(evaluationId).Result;
			for (var i = 0; i < totalGrades; i++)
			{
				var gradeFromBlockchain = contract.GetFunction(Services.Grades.ToString())
					.CallDeserializingToObjectAsync<EthereumGrade>(evaluationId, i)
					.Result;

				results.Add(gradeFromBlockchain);
			};

			return Task.FromResult(results.AsEnumerable());
		}

		public Task<IEnumerable<IGrade>> GetGradesAsync(Filter request)
		{
			var expression = request.AsExpression();
			IEnumerable<Persistence.Grade> grades;
			IEnumerable<Subject> subjects;
			IEnumerable<EthereumGrade> blockchainGrades = Enumerable.Empty<EthereumGrade>();

			using (var db = _dbProvider.GetRepository())
			{
				grades = expression != null
					? db.FetchWhere(expression)
					: db.Fetch<Persistence.Grade>();

				subjects = db.Fetch<Subject>();
			}

			if (grades.Any() && request.Validate)
				blockchainGrades = GetGradesFromBlockchainAsync(request).Result;

			var result = grades.Any()
				? grades.Select(x => x.AsGradeDTO(subjects, blockchainGrades))
				: Enumerable.Empty<IGrade>();

			return Task.FromResult(result);
		}

		public Task<IEnumerable<IGrade>> GetEvaluationGradesAsync(Guid evaluationId)
		{
			IEnumerable<Persistence.Grade> grades;
			IEnumerable<Persistence.User> teachers;

			using (var db = _dbProvider.GetRepository())
			{
				grades = db.FetchWhere<Persistence.Grade>(x => x.EvaluationId == evaluationId);
				teachers = db.FetchWhere<Persistence.User>(x=> x.Role == UserRole.Teacher);
			}

			if(!grades.Any())
				Enumerable.Empty<IGrade>();

			var result = grades.Select(x => x.AsTeacherEvaluationStateDTO(teachers));
			var query = from p in result
						group p by new {
							Evaluation = p.Evaluation,
							IsSigned = p.IsSigned,
							isLocked = p.IsLocked,
							TeacherId = p.TeacherId,
							FullName = p.TeacherFullName
						} into g
						select new Contracts.Grade
						{	
							IsSigned = g.All(x => x.IsSigned),
							IsLocked = g.All(x => x.IsLocked),
							TeacherId = g.Key.TeacherId,
							TeacherFullName = g.Key.FullName, 
							Evaluation = g.Key.Evaluation
						};

			return Task.FromResult(query as IEnumerable<IGrade>);
		}

		public Task<Dictionary<Guid, object>> SetGradesAsync(SetGrades request)
		{
			var results = new Dictionary<Guid, object>();

			using (var db = _dbProvider.GetRepository())
			{
				var grades = db.FetchWhere<Persistence.Grade>(x => x.EvaluationId == request.EvaluationId && x.StudentId == request.StudentId).ToList();

				foreach (var item in request.Grades)
				{
					var subjectId = item.SubjectId;
					var itemResult = HttpStatusCode.NotModified;

					grades.FirstOrDefault(y => y.SubjectId == subjectId)
						.IfNotNull(x =>
						{
							x.SetGrade(item.Grade);
							db.Update(x);
							itemResult = HttpStatusCode.OK;
						});

					results.Add(subjectId, itemResult.ToString());
				};
			}

			return Task.FromResult(results);
		}

		public Task<IEnumerable<Guid>> GetEvaluationLockedAsync(Guid teacherId)
		{
			using (var db = _dbProvider.GetRepository())
			{
				var grades = db.FetchWhere<Persistence.Grade>(x => x.TeacherId == teacherId && x.IsLocked);

				var results = grades.Any()
					? grades.Select(x => x.EvaluationId).Distinct()
					: Enumerable.Empty<Guid>();

				return Task.FromResult(results);
			}
		}
	}
}
