﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Swagger.Annotations.Attributes;
using Swagger.ObjectModel;
using HermaFx;

using QualifyTrust.Daemon.Contracts;

namespace QualifyTrust.Daemon.Api
{
	public class GradesModule : NancyModule
	{
		#region Statics class
		private static class Actions
		{
			public const string GetGradesEvaluation = "grades/evaluations/{evaluationId}";
			public const string GetGradesEvaluationByStudent = "grades/evaluations/{evaluationId}/students/{studentId}/validate/{validate}";
			public const string GetGradesEvaluationByTeacherAndStudent = "grades/evaluations/{evaluationId}/teachers/{teacherId}/students/{studentId}";
			public const string SetGrades = "grades/set";
		}
		#endregion

		#region Private
		private static readonly global::Common.Logging.ILog _log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private static IGradeController _gradeController;
		#endregion

		#region .ctor
		public GradesModule(GradeController gradecontroller) : base("/")
		{
			_gradeController = gradecontroller;

			Get(Actions.GetGradesEvaluation, _ => GetGradesEvaluationsAsync(this.Bind<Filter>()));
			Get(Actions.GetGradesEvaluationByStudent, _ => GetGradesEvaluationsByStudentAsync(this.Bind<Filter>()));
			Get(Actions.GetGradesEvaluationByTeacherAndStudent, _ => GetGradesEvaluationByTeacherAndStudentAsync(this.Bind<Filter>()));
			Post(Actions.SetGrades, _ => SetGradesAsync(this.Bind<SetGrades>()));
		}
		#endregion

		[Route(HttpMethod.Get, Actions.GetGradesEvaluation)]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(IGrade))]
		[Route(Tags = new[] { "Grades" }, Summary = "Get grades by specified filters")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.EvaluationId), Required = true, ParamType = typeof(Guid), Description = "Get all related to Evaluation")]
		private async Task<IEnumerable<IGrade>> GetGradesEvaluationsAsync([RouteParam(ParameterIn.Path)] Filter request)
		{
			_log.Debug($"processing request: {this.Request.Url}");

			return request.EvaluationId.HasValue && (!request.TeacherId.HasValue && !request.StudentId.HasValue)
				? await _gradeController.GetEvaluationGradesAsync(request.EvaluationId.Value)
				: await _gradeController.GetGradesAsync(request);
		}

		[Route(HttpMethod.Get, Actions.GetGradesEvaluationByStudent)]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(IGrade))]
		[Route(Tags = new[] { "Grades" }, Summary = "Get grades by specified filters")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.EvaluationId), Required = true, ParamType = typeof(Guid), Description = "Get all related to Evaluation")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.StudentId), ParamType = typeof(Guid?), Description = "Get all related to StudentId")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.Validate), ParamType = typeof(bool), Description = "Compare grades values with Ethereum grades")]
		private async Task<IEnumerable<IGrade>> GetGradesEvaluationsByStudentAsync([RouteParam(ParameterIn.Path)] Filter request)
		{
			_log.Debug($"processing request: {this.Request.Url}");

			return await _gradeController.GetGradesAsync(request);
		}

		[Route(HttpMethod.Get, Actions.GetGradesEvaluationByTeacherAndStudent)]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(IGrade))]
		[Route(Tags = new[] { "Grades" }, Summary = "Get grades by specified filters")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.EvaluationId), Required = true, ParamType = typeof(Guid), Description = "Get all related to Evaluation")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.TeacherId), ParamType = typeof(Guid?), Description = "Get all related to TeacherId")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.StudentId), ParamType = typeof(Guid?), Description = "Get all related to StudentId")]
		private async Task<IEnumerable<IGrade>> GetGradesEvaluationByTeacherAndStudentAsync([RouteParam(ParameterIn.Path)] Filter request)
		{
			_log.Debug($"processing request: {this.Request.Url}");

			return await _gradeController.GetGradesAsync(request);
		}

		[Route(HttpMethod.Post, "grades/set")]
		[Route(Tags = new[] { "Grades" }, Summary = "Set grades")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(IDictionary<string, object>))]
		private async Task<IDictionary<Guid, object>> SetGradesAsync([RouteParam(ParameterIn.Body)] SetGrades request)
		{
			Guard.IsNotNull(request, nameof(request));
			Guard.IsNotNullNorDefault(request.EvaluationId, nameof(request.EvaluationId));
			Guard.IsNotNullNorDefault(request.StudentId, nameof(request.StudentId));
			Guard.IsNotNullNorDefault(request.Grades, nameof(request.Grades));

			_log.Debug($"processing request: {this.Request.Url} => {this.Request.Body}");

			return await _gradeController.SetGradesAsync(request);
		}
	}
}
