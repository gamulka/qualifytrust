﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualifyTrust.Daemon.Api
{
	public class HomeModule : NancyModule
	{
		public HomeModule()
		{
			var path = "/swagger-ui";

			Get("/", _ => Response.AsRedirect(path));
			Get(path, _ => View["doc", $"{Request.Url.BasePath}/api-docs"]);
		}
	}
}
