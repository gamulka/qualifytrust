﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Swagger.Annotations.Attributes;
using Swagger.ObjectModel;
using HermaFx;

using QualifyTrust.Daemon.Persistence;
using QualifyTrust.Daemon.Contracts;

namespace QualifyTrust.Daemon.Api
{
	public class EvaluationsModule : NancyModule
	{
		#region Statics class
		private static class Actions
		{
			public const string Evaluations = "evaluations";
			public const string Locked = "evaluations/locked/{teacherId}";
			public const string Sign = "evaluation/sign";
			public const string SetLockSate = "evaluation/setLockState";
		}
		#endregion

		#region Private
		private static readonly global::Common.Logging.ILog _log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private static IEvaluationController _evaluationController;
		private static IGradeController _gradeController;
		#endregion

		#region .ctor
		public EvaluationsModule(EvaluationController evaluationController, GradeController gradeController) : base("/")
		{
			_evaluationController = evaluationController;
			_gradeController = gradeController;

			Get(Actions.Evaluations, _ => GetEvaluationsAsync());
			Get(Actions.Locked, _ => GetEvaluationsLockedAsync(this.Bind<Filter>()));
			Post(Actions.Sign, _ => SignAsync(this.Bind<Filter>()));
			Post(Actions.SetLockSate, _ => SetLockStateAsync(this.Bind<Filter>()));
		}
		#endregion

		// Get all evaluations
		[Route(HttpMethod.Get, Actions.Evaluations)]
		[Route(Tags = new[] { "Evaluations" }, Summary = "Get all evaluations")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(Evaluation))]
		private async Task<IEnumerable<Evaluation>> GetEvaluationsAsync()
		{
			_log.Debug($"processing request: {this.Request.Url}");

			return await _evaluationController.GetEvaluationsAsync();
		}

		// Get locked evaluations
		[Route(HttpMethod.Get, Actions.Locked)]
		[Route(Tags = new[] { "Evaluations" }, Summary = "Get locked evaluations")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.TeacherId), ParamType = typeof(Guid?), Description = "Get signed evaluations related to TeacherId")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(Guid))]
		private async Task<IEnumerable<Guid>> GetEvaluationsLockedAsync([RouteParam(ParameterIn.Path)] Filter request)
		{
			_log.Debug($"processing request: {this.Request.Url}");

			return await _gradeController.GetEvaluationLockedAsync(request.TeacherId.Value);
		}

		// Sign evaluation by teacher (Send all grade of teacher to Ethereum)
		[Route(HttpMethod.Post, Actions.Sign)]
		[Route(Tags = new[] { "Evaluations" }, Summary = "Sign an evaluation for specified teacher")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(HttpStatusCode))]
		private async Task<HttpStatusCode> SignAsync([RouteParam(ParameterIn.Body)] Filter request)
		{
			Guard.IsNotNull(request, nameof(request));
			Guard.IsNotNull(request.EvaluationId, nameof(request.EvaluationId));
			Guard.IsNotNull(request.TeacherId, nameof(request.TeacherId));

			_log.Debug($"processing request: {this.Request.Url} => {this.Request.Body}");

			var result = await _evaluationController.SignEvaluationAsync(request.EvaluationId.Value, request.TeacherId.Value)
				? HttpStatusCode.OK
				: HttpStatusCode.NotAcceptable;

			return await Task.FromResult(result);
		}

		// Lock evaluation (by director)
		[Route(HttpMethod.Post, Actions.SetLockSate)]
		[Route(Tags = new[] { "Evaluations" }, Summary = "Send evaluation gredes to Ethereum and lock it", Notes = "Secutiry Scope: [Director]")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(HttpStatusCode))]
		private async Task<HttpStatusCode> SetLockStateAsync([RouteParam(ParameterIn.Body)] Filter request)
		{
			//FIXME: Extract userId and required director role from this.context/claims (@dolivares)

			Guard.IsNotNull(request, nameof(request));
			Guard.IsNotNull(request.EvaluationId, nameof(request.EvaluationId));

			_log.Debug($"processing request: {this.Request.Url} => {this.Request.Body}");

			var result = await _evaluationController.SetLockStateAsync(request.EvaluationId.Value, request.DirectorId.Value, request.State, request.TeacherId)
				? HttpStatusCode.OK
				: HttpStatusCode.NotAcceptable;

			return await Task.FromResult(result);
		}

	}
}
