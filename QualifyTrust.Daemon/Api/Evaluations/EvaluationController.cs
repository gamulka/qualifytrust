﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HermaFx;
using QualifyTrust.Daemon.Persistence;
using QualifyTrust.Daemon.Ethereum;
using Nethereum.Hex.HexTypes;
using System.Threading;
using Nethereum.RPC.Eth.DTOs;

namespace QualifyTrust.Daemon.Api
{
	public interface IEvaluationController
	{
		Task<IEnumerable<Evaluation>> GetEvaluationsAsync();
		Task<bool> SignEvaluationAsync(Guid evaluationId, Guid teacherId);
		Task<bool> SetLockStateAsync(Guid evaluationId, Guid directorId, uint state, Guid? teacherId);
	}

	public class EvaluationController : IEvaluationController
	{
		private static readonly global::Common.Logging.ILog _Log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		IDbProvider _dbProvider;
		private static ISettings _settings;
		private static IEthereumService _ethereumService;
		private static string _owner;
		private static HexBigInteger _gas;

		public EvaluationController(
			ISettings settings,
			IDbProvider dbProvider,
			IEthereumService ethereumService)
		{
			_settings = settings;
			_dbProvider = dbProvider;
			_ethereumService = ethereumService;

			_owner = _settings.Ethereum.AccountAddress;
			_gas = new HexBigInteger(settings.Ethereum.DefaultGas);
		}

		#region Private methods
		private IEnumerable<T> GetItems<T>()
		{
			using (var db = _dbProvider.GetRepository())
			{
				return db.Fetch<T>();
			}
		}

		private void SendToBlockchainAndSignEvaluation(Guid evaluationId, User teacher, IEnumerable<Grade> grades)
		{
			var studentsIds = grades.Select(x => x.StudentId).Distinct().ToList();
			var subjects = GetItems<Subject>();
			var contractsInfo = GetItems<SmartContractInfo>();

			var index = 0;
			var fromAddress = teacher.EthereumAccountAddress;
			var currentNonce = _ethereumService.GetTransactionCountAsync(fromAddress).Result;

			foreach (var studentId in studentsIds)
			{
				var contractInfo = contractsInfo.FirstOrDefault(x => x.UserId == studentId);
				if (contractInfo == null)
					continue;

				var contract = _ethereumService.GetContractAsync(contractInfo.Address, fromAddress).Result;
				var studentGrades = grades.Where(x => x.StudentId == studentId);

				studentGrades.IfNotNull(x => x.ToList().ForEach(grade =>
				{
					_Log.DebugFormat("Setting grades for studenId: {0}, evaluation: {1}", studentId, evaluationId.ToString("n"));

					var subject = subjects.Single(y => y.Id == grade.SubjectId);

					try
					{
						var transaction = new TransactionInput()
						{
							From = fromAddress,
							Gas = _gas,
							Nonce = new HexBigInteger(currentNonce.Value + index++)
						};

						var txHash = contract.GetFunction(Services.SetGrade.ToString())
							.SendTransactionAsync(transaction,
								evaluationId.ToString("n"),
								subject.Identifier,
								grade.GradeValue.Value
							).Result;

						grade.IsSigned = true;
						grade.SignedOn = DateTime.UtcNow;
						grade.TransactionHash = txHash;
					}
					catch (Exception ex)
					{
						_Log.ErrorFormat("Error al setear la notas", ex);
					}
				}));

				try
				{
					var transactionSign = new TransactionInput()
					{
						From = fromAddress,
						Gas = _gas,
						Nonce = new HexBigInteger(currentNonce.Value + index++)
					};

					var txHashSign = contract.GetFunction(Services.SignEvaluation.ToString())
								.SendTransactionAsync(transactionSign,
									evaluationId.ToString("n"),
									fromAddress,
									teacher.Id.ToString(),
									teacher.FullName,
									(string.Join(",", studentGrades.Select(x => x.GradeValue.ToString()))).GetHashCode().ToString()
								).Result;
				}
				catch (Exception ex)
				{
					_Log.ErrorFormat("Error signing evaluation", ex);
				}
			};
		}

		private void SetLockState(Guid evaluationId, User director, IEnumerable<Grade> grades, uint state)
		{
			var index = 0;
			var studentsIds = grades.Select(x => x.StudentId).Distinct().ToList();
			var contractsInfo = GetItems<SmartContractInfo>();
			var fromAddress = director.EthereumAccountAddress;
			var currentNonce = _ethereumService.GetTransactionCountAsync(fromAddress).Result;

			foreach (var studentId in studentsIds)
			{
				var contractInfo = contractsInfo.FirstOrDefault(x => x.UserId == studentId);
				if (contractInfo == null)
					continue;

				try
				{
					var contract = _ethereumService.GetContractAsync(contractInfo.Address, fromAddress).Result;
					var transactionState = new TransactionInput()
					{
						From = fromAddress,
						Gas = _gas,
						Nonce = new HexBigInteger(currentNonce.Value + index++)
					};

					var txHashSign = contract.GetFunction(Services.SetLockState.ToString())
								.SendTransactionAsync(transactionState, evaluationId.ToString("n"), state)
								.Result;

					grades.Where(x => x.StudentId == studentId).ForEach(x =>
					{
						var isLocked = state == 2;
						x.IsLocked = isLocked;
						x.LockedOn = isLocked ? DateTime.UtcNow.Date : DateTime.MinValue;

						if (!isLocked)
						{
							x.IsSigned = false;
							x.SignedOn = DateTime.MinValue;
						}
					});
				}
				catch (Exception ex)
				{
					_Log.ErrorFormat("Error setting evaluation state", ex);
                    throw;
				}
			};
		}
		#endregion

		public Task<IEnumerable<Evaluation>> GetEvaluationsAsync()
		{
			return Task.FromResult(GetItems<Evaluation>());
		}

		public Task<bool> SignEvaluationAsync(Guid evaluationId, Guid teacherId)
		{
			var random = new Random();
			var db = _dbProvider.GetRepository();
			using (var tx = db.GetTransaction())
			{
				var grades = db.FetchWhere<Grade>(x => x.EvaluationId == evaluationId && x.TeacherId == teacherId).ToList();
				var teacher = db.FetchWhere<User>(x => x.Id == teacherId).Single();

				//All grades must be unlocked
				if (!grades.All(x => !x.IsLocked))
					return Task.FromResult(false);

				SendToBlockchainAndSignEvaluation(evaluationId, teacher, grades);

				grades.ForEach(x => db.Update(x));

				tx.Complete();

				var result = grades.All(x => x.IsSigned);
				return Task.FromResult(result);
			}
		}

		public Task<bool> SetLockStateAsync(Guid evaluationId, Guid directorId, uint state, Guid? teacherId = null)
		{
			var db = _dbProvider.GetRepository();
			using (var tx = db.GetTransaction())
			{
				var director = db.FetchWhere<User>(x => x.Id == directorId).Single();
				var grades = teacherId.HasValue
					? db.FetchWhere<Grade>(x => x.EvaluationId == evaluationId && x.TeacherId == teacherId).ToList()
					: db.FetchWhere<Grade>(x => x.EvaluationId == evaluationId).ToList();

				try
				{
					SetLockState(evaluationId, director, grades, state);

					grades.ForEach(x => db.Update(x));

					tx.Complete();
				}
				catch (Exception ex) {
					_Log.ErrorFormat("Error setting lock state...{0}", ex);
                    return Task.FromResult(false);
                }

                return Task.FromResult(true);
			}
		}
	}
}
