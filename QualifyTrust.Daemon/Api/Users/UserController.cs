﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using QualifyTrust.Daemon.Persistence;

namespace QualifyTrust.Daemon.Api
{
	public interface IUserController
	{
		Task<IEnumerable<User>> GetUsersAsync(UserRole role);
		Task<IEnumerable<User>> GetStudentByTeacherAsync(Guid teacherId);
	}

	public class UserController : IUserController
	{
		IDbProvider _dbProvider;

		public UserController(IDbProvider dbProvider)
		{
			_dbProvider = dbProvider;
		}

		public Task<IEnumerable<User>> GetUsersAsync(UserRole role)
		{
			using (var db = _dbProvider.GetRepository())
			{
				var result = db.FetchWhere<User>(x => x.Role == role);

				return Task.FromResult(result.AsEnumerable());
			}
		}

		public Task<IEnumerable<User>> GetStudentByTeacherAsync(Guid teacherId)
		{
			IEnumerable<User> results = new List<User>();

			using (var db = _dbProvider.GetRepository())
			{
				var studentsGrades = db.FetchWhere<Grade>(x => x.TeacherId == teacherId);
				if (studentsGrades.Any())
				{
					var studentsId = studentsGrades.Select(x => x.StudentId).Distinct();
					results = db.Fetch<User>().Where(x => studentsId.Contains(x.Id));
				}
			}

			return Task.FromResult(results.AsEnumerable());
		}
	}
}
