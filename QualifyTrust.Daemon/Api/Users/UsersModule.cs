﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nancy;
using Nancy.Swagger.Annotations.Attributes;
using Swagger.ObjectModel;

using QualifyTrust.Daemon.Contracts;
using QualifyTrust.Daemon.Persistence;
using Nancy.Security;
using Nancy.ModelBinding;

namespace QualifyTrust.Daemon.Api
{
	public class UsersModule : NancyModule
	{
		#region Private
		private static readonly global::Common.Logging.ILog _log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private static IUserController _userController;
		#endregion

		#region .ctor
		public UsersModule(UserController userController) : base("/")
		{
			_userController = userController;

			Get("directors", _ => GetDirectorsAsync());
			Get("teachers", _ => GetTeachersAsync());
			Get("students", _ => GetStudentsAsync());
			Get("students/teachers/{teacherId}", _ => GetStudentByTeacherAsync(this.Bind<Filter>()));

		}
		#endregion

		[Route(HttpMethod.Get, "directors")]
		[Route(Tags = new[] { "Users" }, Summary = "Get all directors")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(IUser))]
		private async Task<IEnumerable<IUser>> GetDirectorsAsync()
		{
			_log.Debug($"processing request: {this.Request.Url}");

			return await _userController.GetUsersAsync(UserRole.Director);
		}

		[Route(HttpMethod.Get, "teachers")]
		[Route(Tags = new[] { "Users" }, Summary = "Get all teachers")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(IUser))]
		private async Task<IEnumerable<IUser>> GetTeachersAsync()
		{
			_log.Debug($"processing request: {this.Request.Url}");

			return await _userController.GetUsersAsync(UserRole.Teacher);
		}

		[Route(HttpMethod.Get, "students")]
		[Route(Tags = new[] { "Users" }, Summary = "Get all students")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(IUser))]
		private async Task<IEnumerable<IUser>> GetStudentsAsync()
		{
			this.RequiresAuthentication();

			_log.Debug($"processing request: {this.Request.Url}");

			return await _userController.GetUsersAsync(UserRole.Student);
		}

		[Route(HttpMethod.Get, "students/teachers/{teacherId}")]
		[SwaggerResponse(HttpStatusCode.OK, Message = "OK", Model = typeof(Persistence.User))]
		[Route(Tags = new[] { "Users" }, Summary = "Get students of specified teacher")]
		[RouteParam(ParameterIn.Path, name: nameof(Filter.TeacherId), Required = true, ParamType = typeof(Guid), Description = "Get all related to TeacherId")]
		private async Task<IEnumerable<Persistence.User>> GetStudentByTeacherAsync([RouteParam(ParameterIn.Path)] Filter request)
		{
			_log.Debug($"processing request: {this.Request.Url}");

			return await _userController.GetStudentByTeacherAsync(request.TeacherId.Value);
		}
	}
}
