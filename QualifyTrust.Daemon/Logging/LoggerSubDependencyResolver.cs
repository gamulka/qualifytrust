﻿using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;

using Common.Logging;

namespace QualifyTrust.Daemon.Logging
{
	public class LoggerSubDependencyResolver : ISubDependencyResolver
	{
		public bool CanResolve(CreationContext context, ISubDependencyResolver contextHandlerResolver, ComponentModel model, DependencyModel dependency)
		{
			return dependency.TargetType == typeof(ILog);
		}

		public object Resolve(CreationContext context, ISubDependencyResolver contextHandlerResolver, ComponentModel model, DependencyModel dependency)
		{
			return CanResolve(context, contextHandlerResolver, model, dependency) && dependency.TargetType == typeof(ILog)
				? LogManager.GetLogger(model.Implementation)
				: null;
		}
	}
}
