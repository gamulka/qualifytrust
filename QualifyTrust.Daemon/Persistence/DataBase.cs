﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QualifyTrust.Daemon.Persistence
{
	public class Database : NPoco.Database, IDisposable
	{
		public Database(ISettings settings)
			: base(settings.ConnectionString, new NPoco.DatabaseTypes.SQLiteDatabaseType())
		{
		}

		void IDisposable.Dispose()
		{
			KeepConnectionAlive = false; // XXX: Avoid connection leaks
			base.Dispose();
		}
	}
}
