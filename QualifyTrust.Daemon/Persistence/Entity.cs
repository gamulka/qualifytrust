﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualifyTrust.Daemon.Persistence
{
	public interface IEntity
	{
		Guid Id { get; set; }
		DateTime CreatedOn { get; set; }
	}

	public class Entity : IEntity
	{
		public Guid Id { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime ModifiedOn { get; set; }
	}
}
