﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NPoco;

namespace QualifyTrust.Daemon.Persistence
{
	internal static class DatabaseExtensions
	{
		public static bool TableExists(this IDatabase db, string table)
		{
			return db.ExecuteScalar<bool>($@"SELECT EXISTS (SELECT 1 FROM sqlite_master WHERE type='table' AND name='{table}');");
		}

		public static void TruncateTable(this IDatabase db, IEntity entity)
		{
			db.ExecuteScalar<bool>($@"DELETE FROM '{entity.TableName()}';");
		}
	}
}
