﻿using System;
using NPoco;
using HermaFx;
using HermaFx.Utils;

namespace QualifyTrust.Daemon.Persistence
{
	[TableName("Grades")]
	[PrimaryKey("Id", AutoIncrement = false)]
	public class Grade : Entity
	{
		#region Properties
		public Guid EvaluationId { get; protected set; }
		public Guid StudentId { get; protected set; }
		public Guid TeacherId { get; protected set; }
		public Guid SubjectId { get; protected set; }
		public int? GradeValue { get; set; }
		public bool IsSigned { get; set; }
		public DateTime? SignedOn { get; set; }
		public bool IsLocked { get; set; }
		public DateTime? LockedOn { get; set; }
		public string TransactionHash { get; set; }

		#endregion

		#region Static Factory
		internal static Grade CreateFrom(Evaluation evaluation, User student, User teacher, Subject subject, int? grade)
		{
			Guard.IsNotNull(evaluation, nameof(evaluation));
			Guard.IsNotNull(student, nameof(student));
			Guard.IsNotNull(teacher, nameof(teacher));
			Guard.IsNotNull(subject, nameof(subject));

			return new Grade()
			{
				Id = AdvancedGuidGenerator.GenerateComb(),
				CreatedOn = DateTime.UtcNow,
				EvaluationId = evaluation.Id,
				StudentId = student.Id,
				TeacherId = teacher.Id,
				SubjectId = subject.Id,
				GradeValue = grade
			};
		}

		public void SetGrade(int grade)
		{
			this.GradeValue = grade;
			this.ModifiedOn = DateTime.UtcNow;
		}
		#endregion
	}
}
