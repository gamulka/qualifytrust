﻿using NPoco;
using HermaFx;
using HermaFx.Utils;
using QualifyTrust.Daemon.Contracts;
using System;

namespace QualifyTrust.Daemon.Persistence
{
	[TableName("Subjects")]
	[PrimaryKey("Id", AutoIncrement = false)]
	public class Subject : Entity
	{
		#region Properties
		public virtual string Name { get; protected set; }
		public virtual string Identifier { get; protected set; }
		#endregion

		#region Static Factory
		public static Subject Create(string identifier ,string name)
		{
			Guard.IsNotNullNorEmpty(name, nameof(name));
			Guard.IsNotNullNorEmpty(identifier, nameof(identifier));

			return new Subject()
			{
				Id = AdvancedGuidGenerator.GenerateComb(),
				CreatedOn = DateTime.UtcNow,
				Identifier = identifier,
				Name = name
			};
		}
		#endregion
	}
}
