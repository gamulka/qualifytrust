﻿using NPoco;
using HermaFx;
using HermaFx.Utils;
using QualifyTrust.Daemon.Contracts;
using System;

namespace QualifyTrust.Daemon.Persistence
{
	public enum UserRole
	{
		Director = 1,
		Teacher = 2,
		Student = 3
	}

	[TableName("Users")]
	[PrimaryKey("Id", AutoIncrement = false)]
	public class User : Entity, IUser
	{
		#region Properties
		public virtual int Index { get; protected set; }
		public virtual string FirstName { get; protected set; }
		public virtual string LastName { get; protected set; }
		public virtual int Sex { get; protected set; }
		public virtual string Photo { get; protected set; }
		public virtual string UserName { get; protected set; }
		public virtual string Password { get; protected set; }
		public virtual UserRole Role { get; protected set; }
		public virtual string EthereumAccountAddress { get; protected set; }
		[Ignore]
		public string FullName => $"{FirstName} {LastName}";
		#endregion

		#region Static Factory
		internal static User Create(
			int index,
			string firstName,
			string lastName,
			int sex,
			string photo,
			string userName,
			string password,
			UserRole role,
			string ethereumAccountAddress)
		{
			Guard.IsNotNullNorEmpty(firstName, nameof(firstName));
			Guard.IsNotNullNorEmpty(lastName, nameof(lastName));
			Guard.IsNotNullNorEmpty(photo, nameof(photo));
			Guard.IsNotNullNorEmpty(userName, nameof(userName));
			Guard.IsNotNullNorEmpty(password, nameof(password));
			Guard.IsNotNullNorEmpty(password, nameof(password));
			Guard.IsNotNullNorEmpty(ethereumAccountAddress, nameof(ethereumAccountAddress));

			return new User()
			{
				Id = AdvancedGuidGenerator.GenerateComb(),
				CreatedOn = DateTime.UtcNow,
				Index = index,
				FirstName = firstName,
				Sex = sex,
				Photo = photo,
				LastName = lastName,
				UserName = userName,
				Password = password,
				Role = role,
				EthereumAccountAddress = ethereumAccountAddress
			};
		}
		#endregion
	}
}
