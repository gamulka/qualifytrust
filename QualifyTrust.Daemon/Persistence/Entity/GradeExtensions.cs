﻿using QualifyTrust.Daemon.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace QualifyTrust.Daemon.Persistence
{
	public static class GradeExtensions
	{
		private static readonly global::Common.Logging.ILog _Log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public static IGrade AsGradeDTO(this Grade grade, IEnumerable<Subject> subjects, IEnumerable<EthereumGrade> ethereumGrades)
		{
			var isInEthereum = ethereumGrades.Any(x => x.Id == subjects.Single(_ => _.Id == grade.SubjectId).Identifier);
			var isValid = isInEthereum
				? ethereumGrades.Any(x => x.Id == subjects.Single(_ => _.Id == grade.SubjectId).Identifier && grade.GradeValue.Value == x.Value).ToString()
				: string.Empty;

			return new Contracts.Grade()
			{
				Evaluation = grade.EvaluationId.ToString(),
				GradeValue = grade.GradeValue,
				Id = grade.Id,
				StudentId = grade.StudentId,
				SubjectId = grade.SubjectId.ToString(),
				SubjectName = subjects.Single(x=> x.Id == grade.SubjectId).Name,
				TeacherId = grade.TeacherId,
				IsLocked = grade.IsLocked,
				IsSigned = grade.IsSigned,
				ModifiedOn = grade.ModifiedOn,
				SignedOn = grade.SignedOn,
				TxHash = grade.TransactionHash,
				IsValid = isValid,
			};
		}

		public static IGrade AsTeacherEvaluationStateDTO(this Grade grade, IEnumerable<User> teachers)
		{
			return new Contracts.Grade()
			{
				Evaluation = grade.EvaluationId.ToString(),
				GradeValue = grade.GradeValue,
				Id = grade.Id,
				StudentId = grade.StudentId,
				SubjectId = grade.SubjectId.ToString(),
				TeacherFullName = teachers.Where(x => x.Id == grade.TeacherId).Select(x=> $"{x.FirstName} {x.LastName}").FirstOrDefault(),
				TeacherId = grade.TeacherId,
				IsLocked = grade.IsLocked,
				IsSigned = grade.IsSigned,
				ModifiedOn = grade.ModifiedOn,
				SignedOn = grade.SignedOn
			};
		}

		public static Expression<Func<Grade, bool>> AsExpression(this Filter request)
		{
			Expression<Func<Grade, bool>> expression1 = x => x.EvaluationId == request.EvaluationId && x.TeacherId == request.TeacherId && x.StudentId == request.StudentId;
			Expression<Func<Grade, bool>> expression2 = x => x.TeacherId == request.TeacherId.Value && x.StudentId == request.StudentId.Value;
			Expression<Func<Grade, bool>> expression3 = x => x.TeacherId == request.TeacherId;
			Expression<Func<Grade, bool>> expression4 = x => x.StudentId == request.StudentId.Value;
			Expression<Func<Grade, bool>> expression5 = x => x.StudentId == request.StudentId.Value && x.EvaluationId == request.EvaluationId.Value;
			Expression<Func<Grade, bool>> expression6 = x => x.TeacherId == request.TeacherId.Value && x.EvaluationId == request.EvaluationId.Value;

			if (request.TeacherId.HasValue && request.StudentId.HasValue && request.EvaluationId.HasValue)
				return expression1;
			else if (request.TeacherId.HasValue && request.StudentId.HasValue)
				return expression2;
			else if (request.TeacherId.HasValue && request.EvaluationId.HasValue)
				return expression6;
			else if (request.StudentId.HasValue && request.EvaluationId.HasValue)
				return expression5;
			else if (request.TeacherId.HasValue)
				return expression3;
			else if (request.StudentId.HasValue)
				return expression4;

			return null;
		}
	}
}
