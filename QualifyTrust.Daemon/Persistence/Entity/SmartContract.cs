﻿using System;
using NPoco;
using HermaFx;
using HermaFx.Utils;
using QualifyTrust.Daemon.Contracts;

namespace QualifyTrust.Daemon.Persistence
{
	[TableName("SmartContractsInfo")]
	[PrimaryKey("Id", AutoIncrement = false)]
	public class SmartContractInfo : Entity
	{
		public Guid UserId { get; set; }
		public string TransactionHash { get; protected set; }
		public string Address { get; protected set; }

		#region Static factory
		
		public static SmartContractInfo Create(Guid userId, string transactionHash, string contractAddress = null)
		{
			Guard.IsNotNullNorDefault(userId, nameof(userId));
			Guard.IsNotNullNorEmpty(transactionHash, nameof(transactionHash));

			return new SmartContractInfo()
			{
				Id = AdvancedGuidGenerator.GenerateComb(),
				CreatedOn = DateTime.UtcNow,
				UserId = userId,
				TransactionHash = transactionHash,
				Address = contractAddress
			};
		}

		#endregion
	}
}
