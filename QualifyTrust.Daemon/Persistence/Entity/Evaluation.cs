﻿using NPoco;
using HermaFx;
using HermaFx.Utils;
using QualifyTrust.Daemon.Contracts;
using System;

namespace QualifyTrust.Daemon.Persistence
{
	[TableName("Evaluations")]
	[PrimaryKey("Id", AutoIncrement = false)]
	public class Evaluation : Entity
	{
		#region Properties
		public virtual string Name { get; protected set; }
		#endregion

		#region Static Factory
		public static Evaluation Create(string name)
		{
			Guard.IsNotNullNorEmpty(name, nameof(name));

			return new Evaluation()
			{
				Id = AdvancedGuidGenerator.GenerateComb(),
				CreatedOn = DateTime.UtcNow,
				Name = name
			};
		}
		#endregion
	}
}
