﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

using HermaFx.DesignPatterns;

namespace QualifyTrust.Daemon.Persistence
{
	public interface IDbProvider : IService
	{
		Repository GetRepository();
	}

	public class DbProvider : IDbProvider
	{
		private readonly ISettings _settings;
		public static string connectionString;

		public DbProvider(ISettings settings)
		{
			_settings = settings;
		}

		public Repository GetRepository() => new Repository(_settings);
	}
}
