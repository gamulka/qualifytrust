﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using HermaFx;
using QualifyTrust.Daemon.Properties;

namespace QualifyTrust.Daemon.Persistence
{
	public class Repository : Database
	{
		private static readonly global::Common.Logging.ILog _Log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private readonly ISettings _settings;

		public Repository(ISettings settgins) : base(settgins)
		{
			_settings = settgins;
		}

		public Repository GetDatabase() => new Repository(_settings);

		public bool Exists(Guid id)
		{
			return Exists<IEntity>(id);
		}

		public IEntity Single(Guid id)
		{
			return SingleById<IEntity>(id);
		}

		public IEnumerable<IEntity> GetAll()
		{
			return Fetch<IEntity>();
		}

		public IEnumerable<IEntity> Fetch(Expression<Func<IEntity, bool>> expression)
		{
			return FetchWhere(expression);
		}
	}
}
