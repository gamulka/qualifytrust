﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualifyTrust.Daemon.Persistence
{
	public static class EntityExtensions
	{
		private static string GetTableNameFor(IEntity entity)
		{
			var attributes = entity.GetType().GetCustomAttributes(true);
			foreach (var attribute in attributes)
			{
				var tableNameAttribute = attribute as TableNameAttribute;
				if (tableNameAttribute != null)
				{
					return tableNameAttribute.Value;
				}
			}

			return null;
		}

	public static string TableName(this IEntity entity)
	{
		return GetTableNameFor(entity);
	}
}
}
