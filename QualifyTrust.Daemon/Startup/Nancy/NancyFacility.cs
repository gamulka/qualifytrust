﻿using System;

using Castle.Windsor;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using Nancy.Hosting.Self;
using Nancy.Conventions;

namespace QualifyTrust.Daemon.Startup
{
	public class NancyFacility : AbstractFacility
	{
		private static string WebServiceName => "QualifyTrust";

		private static readonly global::Common.Logging.ILog _Log = global::Common.Logging.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly object _NancyDataAnnotationsDependency = typeof(Nancy.Validation.DataAnnotations.DataAnnotationsValidatorAdapter); //< Ensure compile-time dependency
		private static readonly object _NancyDataAnnotationsExtensionsDependency = typeof(Nancy.Validation.DataAnnotations.Extensions.BaseValidatorAdapter<>); //< Ensure compile-time dependency

		private readonly ISettings _settings;
		private IWindsorContainer _container;
		private IWindsorContainer _nancyContainer; //< Nancy's sub-container here...

		private NancyHost _host;

		public NancyFacility(ISettings settings, IWindsorContainer container)
		{
			_settings = settings;
			_container = container;
		}

		protected override void Init()
		{
			var url = _settings.Url;
			_Log.InfoFormat("Starting {0} WebService host at {1}...", WebServiceName, url);

			var configuration = new HostConfiguration();
			configuration.UrlReservations.CreateAutomatically = true;

			_nancyContainer = new WindsorContainer() { Parent = _container };

			var bootstrapper = new NancyBootstrapper(_nancyContainer);
			_host = new NancyHost(bootstrapper, configuration, new Uri(url));

			Kernel.Register(Component.For<NancyHost>().Instance(_host));
		}

		protected override void Dispose()
		{
			_host?.Dispose(); _host = null;
			_container?.Dispose(); _container = null;
			base.Dispose();
		}
	}
}
