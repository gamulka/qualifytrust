﻿
using Castle.Windsor;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;

namespace QualifyTrust.Daemon.Startup
{
	public class NancyInstaller : IWindsorInstaller
	{
		private readonly ISettings _settings;

		public NancyInstaller(ISettings settings)
		{
			_settings = settings;
		}

		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.AddFacility(new NancyFacility(_settings, container));
		}
	}
}
