﻿using System;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

using Nancy.Swagger.Services;
using Nancy.Bootstrappers.Windsor;
using Nancy.Configuration;
using Nancy.Conventions;
using Nancy.Swagger.Annotations;
using Nancy.Authentication.Stateless;
using Swagger.ObjectModel;

using HermaFx;
using Nancy.Bootstrapper;
using Nancy;
using QualifyTrust.Daemon.AuthService;

namespace QualifyTrust.Daemon.Startup
{
	public class NancyBootstrapper : WindsorNancyBootstrapper
	{
		private readonly IWindsorContainer _container;

		public NancyBootstrapper(IWindsorContainer container)
		{
			Guard.IsNotNull(container, nameof(container));

			//JsonSettings.MaxJsonLength = int.MaxValue;

			#region Nancy Swagger
			SwaggerAnnotationsConfig.ShowOnlyAnnotatedRoutes = true;

			Nancy.Swagger.SwaggerTypeMapping.AddTypeMapping(typeof(Guid), typeof(string));
			Nancy.Swagger.SwaggerTypeMapping.AddTypeMapping(typeof(Guid?), typeof(string));
			Nancy.Swagger.SwaggerTypeMapping.AddTypeMapping(typeof(DateTime), typeof(string));
			Nancy.Swagger.SwaggerTypeMapping.AddTypeMapping(typeof(DateTime?), typeof(string));

			SwaggerMetadataProvider.SetInfo(
				title: "Nancy Swagger QualifyTrust",
				version: "v1",
				desc: "Interacting with blockchain as service"
			);
			#endregion

			_container = container;
		}

		public override INancyEnvironment GetEnvironment()
		{
			return _container.Resolve<INancyEnvironment>();
		}

		protected override IWindsorContainer GetApplicationContainer()
		{
			return _container;
		}

		protected override INancyEnvironmentConfigurator GetEnvironmentConfigurator()
		{
			return _container.Resolve<INancyEnvironmentConfigurator>();
		}

		protected override void RegisterNancyEnvironment(IWindsorContainer container, INancyEnvironment environment)
		{
			container.Register(Component.For<INancyEnvironment>().Instance(environment));
		}

		protected override void ConfigureConventions(NancyConventions nancyConventions)
		{
			nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("swagger-ui"));
		}

		protected override void RequestStartup(IWindsorContainer container, IPipelines pipelines, NancyContext context)
		{
			var authService = _container.Resolve<IAuthService>();
			var configuration = new StatelessAuthenticationConfiguration(nancyContext =>
				{
					const string key = "Bearer ";
					string accessToken = null;

					if (nancyContext.Request.Headers.Authorization.StartsWith(key))
					{
						accessToken = nancyContext.Request.Headers.Authorization.Substring(key.Length);
					}

					if (string.IsNullOrWhiteSpace(accessToken))
						return null;

					return authService.GetUserFromAccessTokenAsync(accessToken).Result;
				});

			AllowAccessToConsumingSite(pipelines);

			StatelessAuthentication.Enable(pipelines, configuration);
		}

		static void AllowAccessToConsumingSite(IPipelines pipelines)
		{
			pipelines.AfterRequest.AddItemToEndOfPipeline(x =>
			{
				x.Response.Headers.Add("Access-Control-Allow-Origin", "*");
				x.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
				x.Response.Headers.Add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH,OPTIONS");
				x.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
			});
		}
	}
}
