﻿using System.Configuration;

using Castle.Core;
using Castle.Windsor;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using HermaFx.Settings;
using HermaFx.DesignPatterns;
using QualifyTrust.Daemon.Api;

namespace QualifyTrust.Daemon.Startup
{
	public class MainInstaller : IWindsorInstaller
	{
		#region IWindsorInstaller Members

		private readonly ISettings _settings;
		public MainInstaller(ISettings settings)
		{
			_settings = settings;
		}

		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.AddFacility<TypedFactoryFacility>();
			container.AddFacility<StartableFacility>(x => x.DeferredStart());
			container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));
			container.Kernel.Resolver.AddSubResolver(new Logging.LoggerSubDependencyResolver());

			container.Register(Component.For<IWindsorContainer>().Instance(container));
			container.Register(Component.For<Service>().LifestyleSingleton());
			container.Register(Component.For<ISettings>()
				.UsingFactoryMethod(() => new SettingsAdapter().Create<ISettings>(ConfigurationManager.AppSettings))
				.LifestyleSingleton()
			);
			container.Register(
				Classes.FromThisAssembly()
					.BasedOn<IService>().WithServiceFromInterface()
					.LifestyleTransient()
			);

			// TODO: Make this dynamic implement interface (@dolvaes)
			// Domain inyection 
			container.Register(Component.For<EvaluationController>().LifestyleSingleton());
			container.Register(Component.For<UserController>().LifestyleSingleton());
			container.Register(Component.For<GradeController>().LifestyleSingleton());
			container.Register(Component.For<BootController>().LifestyleSingleton());
		}

		#endregion
	}
}
