﻿using System.ComponentModel.DataAnnotations;

using HermaFx.Settings;
using HermaFx.DataAnnotations;
using System.Configuration;

namespace QualifyTrust.Daemon
{
	public interface ISettings
	{
		[Required]
		string Url { get; set; }

		[Required]
		[SettingsDescription("DB connection string")]
		string ConnectionString { get; set; }

		[Settings, ValidateObject]
		IAuthSettings Auth { get; set; }

		[Settings, ValidateObject]
		IEthereumSettings Ethereum { get; set; }
	}
}
