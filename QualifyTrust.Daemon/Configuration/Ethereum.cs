﻿using System;
using System.Configuration;
using System.ComponentModel.DataAnnotations;
using HermaFx;
using HermaFx.DataAnnotations;

namespace QualifyTrust.Daemon
{
	public interface IEthereumSettings
	{
		[SettingsDescription("Start Ethereum service in mode PRO or DEV")]
		[DefaultSettingValue("false")]
		bool UseModePro { get; set; }

		[SettingsDescription("Deploy from JSON file or ABI/Bytecode files")]
		[DefaultSettingValue("false")]
		bool DeployFromJSON { get; set; }

		[SettingsDescription("Internet address to connect with Ethereum blockchain development node.")]
		string ServerUrlDev { get; set; }

		[SettingsDescription("Internet address to connect with Ethereum blockchain node.")]
		string ServerUrlPro { get; set; }

		[Required]
		[SettingsDescription("Wordlist to generate accounts")]
		string Mnemonic { get; set; }

		[Required]
		[SettingsDescription("Main account")]
		string AccountAddress { get; set; }

		[DefaultSettingValue("5000000")]
		int DefaultGas { get; set; }

		#region Contracts paths

		[SettingsDescription("Contracts path for students contract.")]
		string ContractJSONPath { get; set; }

		[SettingsDescription("ABI path for students contract.")]
		string ContractABIPath { get; set; }

		[SettingsDescription("Bitecode path for students contract.")]
		string ContractBytecodePath { get; set; }

		#endregion

	}
}
