﻿using System;
using System.Configuration;
using System.ComponentModel.DataAnnotations;

namespace QualifyTrust.Daemon
{
	public interface IAuthSettings
	{
		[Required]
		[SettingsDescription("Used to sign the JWT")]
		string SecretKey { get; set; }

		[Required]
		[SettingsDescription("Identifies the recipients that the JWT is intended for.")]
		string AudienceToken { get; set; }

		[Required]
		[SettingsDescription("Identifies the principal that issued the JWT.")]
		string IssuerToken { get; set; }

		[SettingsDescription("Expiration time, identifies the expiration time on or after which the JWT MUST NOT be accepted (in minutes) default = 6H")]
		int ExpireMinutes { get; set; }
	}
}
