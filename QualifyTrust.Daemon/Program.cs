﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using Castle.Windsor;

using Topshelf;

using HermaFx.Settings;

namespace QualifyTrust.Daemon
{
	public class Program
	{
		private static global::NLog.Logger _Log = null;

		public static string ProgramName { get { return "QualifyTrust.Daemon"; } }
		public static Version ProgramVersion { get { return typeof(Program).Assembly.GetName().Version; } }

		private static void SetupLogging()
		{
			NLog.LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(
				Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "NLog.config")
			);

			Common.Logging.LogManager.Adapter = new Common.Logging.NLog.NLogLoggerFactoryAdapter(
					new Common.Logging.Configuration.NameValueCollection()
					{
						{ "configType", "EXTERNAL" }
					});

			_Log = global::NLog.LogManager.GetCurrentClassLogger();
		}

		private static void EnsureDb(ISettings settings)
		{
			var dbFile = "QualifyTrust.sqlite";
			var dbPath = Path.Combine(Environment.CurrentDirectory, dbFile);
			var sourceDbPath = Path.Combine(Environment.CurrentDirectory,"Resources", dbFile);
			if (!File.Exists(dbPath))
			{
				_Log.Debug($"Copying db file to path: {dbPath}...");

				File.Copy(sourceDbPath, dbPath);

				_Log.Debug($"Copying db file to path: {dbPath}...[DONE]");
			}
		}

		private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			_Log.Fatal(e.ExceptionObject as Exception, "Unhandled exception!");
		}

		private static void MainInternal(string[] args)
		{
			var settings = new SettingsAdapter().Create<ISettings>(ConfigurationManager.AppSettings);
			
			SetupLogging();
			EnsureDb(settings);

			_Log.Info("{0} v{1} starting.", ProgramName, ProgramVersion);
			_Log.Info("CommandLine: {0}", Environment.CommandLine);
			_Log.Info("CommandLineArgs: {0}", Environment.GetCommandLineArgs().Aggregate((cur, next) => cur + "," + next));
			_Log.Info("CurrentBaseDirectory: {0}", AppDomain.CurrentDomain.BaseDirectory);
			_Log.Info("ConfigurationFile: {0}", AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledException);

			//Start the container
			using (var container = new WindsorContainer())
			{
				container.Install(
					new Startup.MainInstaller(settings),
					new Startup.NancyInstaller(settings)
				);

				var service = container.Resolve<Service>();

				//Start the service
				HostFactory.Run(x =>
				{
					x.UseNLog();
					x.UseLinuxIfAvailable();
					x.SetServiceName(ProgramName);
					x.SetDescription($"{ProgramName} Service");
					x.StartAutomatically();
					x.RunAsLocalSystem();
					x.Service(() => service);
				});

				container.Release(service);
			}
		}

		public static void Main(string[] args)
		{
			try
			{
				SetupLogging();
				MainInternal(args);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine("ERROR: " + ex.Message);
				Console.Error.WriteLine(ex.StackTrace);
				_Log.Fatal(ex, "Initialization error!");

				Environment.Exit(255);
			}

			Environment.Exit(0);
		}
	}
}
