export default [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('./views/Index.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/director',
      component: () => import('./views/Director/Index.vue'),
      meta: { requiresAuth: true, requiresRole: true, role: 1 },
    },
    {
      path: '/teacher',
      component: () => import('./views/Teacher/Index.vue'),
      meta: { requiresAuth: true, requiresRole: true, role: 2 }
    },
    {
      path: '/student',
      component: () => import('./views/Student/Index.vue'),
      meta: { requiresAuth: true, requiresRole: true, role: 3 }
    }
  ]