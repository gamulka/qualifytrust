export default {
  methods: {
    gradeToNumeric: function (value) {
      return value.toString().replace('NP', '-1')
    },
    gradeToString: function (value) {
      return value.toString().replace('-1', 'NP')
    },
    getGradeResultText: function (value) {
      var result="Insuficiente"

      if(value >= 9 && value <= 10) {
        return result = "Sobresaliente"
      }
      if(value >= 7 && value <= 8) {
        return result = "Notable"
      }
      if(value == 6) {
        return result = "Bien"
      }
      if(value == 5) {
        return result = "Suficiente"
      }
      if(value == -1) {
        return result = "No presentado"
      }
      return result
    },
    getGradeResultColor: function (value) {
      var result="danger"

      if(value >= 9 && value <= 10) {
        return result = "success"
      }
      if(value >= 7 && value <= 8) {
        return result = "primary"
      }
      if(value == 6) {
        return result = "info"
      }
      if(value == 5) {
        return result = "warning"
      }
      if(value == -1) {
        return result = "secondary"
      }
      return result
    },
    getValidationColor: function (value) {
      var result="light"

      if(value == "True") {
        return result = "success"
      }
      if(value == "False") {
        return result = "danger"
      }
      return result
    },
    getValidationText: function (value) {
      var result=""

      if(value == "True") {
        return result = "Válido"
      }
      if(value == "False") {
        return result = "No válido"
      }
      return result
    }
  }
}
