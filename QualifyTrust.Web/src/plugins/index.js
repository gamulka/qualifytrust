import './axios';
import './bootstrap-vue';
import './vee-validate';
import './vue-icon';
import './event-bus';
import './vue-toasted';