import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

// Default config options
const baseURL = 'http://localhost:2380'
const defaultOptions = {
    baseURL: baseURL,
    headers: {
        'Content-Type': 'application/json',
    },
};

// Create instance
let instance = axios.create(defaultOptions);

// Set the AUTH token for any request
instance.interceptors.request.use(function (config) {
    const token = localStorage.getItem('authToken');
    config.headers.Authorization = token ? `Bearer ${token}` : '';
    return config;
});

Vue.use(VueAxios, instance)