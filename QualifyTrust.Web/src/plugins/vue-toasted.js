import Vue from 'vue'
import Toasted from 'vue-toasted';

 var options = { 
    theme: "toasted-primary", 
    position: "top-center", 
    duration : 2000,
    fullWidth: false
  };
Vue.use(Toasted, options)