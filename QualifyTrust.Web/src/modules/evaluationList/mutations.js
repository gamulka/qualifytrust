export function setEvaluations(state, data) {
  state.evaluations = data
}

export function setEvaluationsLocked(state, data) {
  state.evaluationsLocked = data
}