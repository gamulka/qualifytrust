import Vue from 'vue'

export async function getEvaluations(context) {
  try {
    context.commit('setLoading', true, { root: true })
    const { data } = await Vue.axios({
      url: 'evaluations/'
    })
    context.commit('setEvaluations', data)
  } catch (e) {
    Vue.toasted.show('Error al cargar las evaluaciones', {type: 'error'});
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}

export async function getEvaluationsLocked(context, params) {
  try {
    context.commit('setLoading', true, { root: true })
    const { data } = await Vue.axios({
      url: 'evaluations/locked/' + params.userId
    })
    context.commit('setEvaluationsLocked', data)
  } catch (e) {
    Vue.toasted.show('Error al cargar las evaluaciones cerradas', {type: 'error'});
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}