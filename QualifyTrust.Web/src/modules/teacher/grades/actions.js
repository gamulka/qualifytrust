import Vue from 'vue'

export async function getGrades(context, params) {
  try {
    context.commit('setLoading', true, { root: true })
    const { data } = await Vue.axios.get(
      'grades/evaluations/' + params.EvaluationId +
      '/teachers/' + params.TeacherId +
      '/students/' + params.StudentId
    )
    context.commit('setGrades', {
      evaluationId: params.EvaluationId,
      teacherId: params.TeacherId,
      studentId: params.StudentId,
      data: data
    })
  } catch (e) {
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}

export function resetGrades(context) {
  context.commit('resetGrades')
}

export async function setGrade(context, params) {
  var state = context.state
  var evaluationId = state.evaluationId
  var teacherId = state.teacherId
  var studentId = state.studentId

  try {
    context.commit('setLoading', true, { root: true })
    await Vue.axios({
      method: 'POST',
      url: '/grades/set',
      data: {
        evaluationId: evaluationId,
        teacherId: teacherId,
        studentId: studentId,
        grades: [ 
          {subjectId: params.subjectId, grade: params.grade }
        ]
      }
    })
    Vue.toasted.show('La petición se ha realizado correctamente !!', {type: 'success'});
  } catch (e) {
    Vue.toasted.show('Error al realizar la petición !!', {type: 'error'});
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }

  // Reaload
  await getGrades(context, {
    EvaluationId: evaluationId,
    TeacherId: teacherId,
    StudentId: studentId
  })
}