export function setGrades(state, params) {
  state.evaluationId = params.evaluationId
  state.teacherId = params.teacherId
  state.studentId = params.studentId
  state.grades = params.data
}

export function resetGrades(state) {
  state.grades = []
}