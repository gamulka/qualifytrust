import Vue from 'vue'

export async function sign(context, params) {

  try {
    context.commit('setLoading', true, { root: true })
    await Vue.axios({
      method: 'POST',
      url: '/evaluation/sign',
      data: {
        evaluationId: params.evaluationId,
        teacherId: params.teacherId
      }
    }).then(function (response) {
      context.commit('setSuccess', 'La evaluación se ha firmado correctamente!', { root: true })
    })
  } catch (e) {
    context.commit('setError', 'Error al realizar la firma de la evaluación', { root: true })
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}