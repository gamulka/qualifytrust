import Vue from 'vue'

export async function getGrades(context, params) {
  try {
    context.commit('setLoading', true, { root: true })
    const { data } = await Vue.axios.get(
      'grades/evaluations/' + params.EvaluationId +
      '/students/' + params.StudentId +
      '/validate/' + params.Validate
    )
    context.commit('setGrades', data)
  } catch (e) {
    Vue.toasted.show('Error al cargar las notas', {type: 'error'});
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}

export function resetGrades(context) {
  context.commit('resetGrades')
}