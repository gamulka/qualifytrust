export function setGrades(state, data) {
  state.grades = data
}

export function resetGrades(state) {
  state.grades = []
}