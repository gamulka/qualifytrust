import Vue from 'vue'

export async function getStudents(context, params) {
  try {
    context.commit('setLoading', true, { root: true })
    const { data } = await Vue.axios.get('students/teachers/' + params.TeacherId)
    context.commit('setStudents', data)
  } catch (e) {
    Vue.toasted.show('Error al cargar la lista de estudiantes', {type: 'error'});
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}