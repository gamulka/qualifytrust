import Vue from 'vue'

export async function getEvaluationState(context, params) {
  try {
    context.commit('setLoading', true, { root: true })
    const { data } = await Vue.axios.get(
      '/grades/evaluations/' + params.EvaluationId
    )
    context.commit('setEvaluationState', {
      evaluationId: params.EvaluationId,
      data: data
    })
  } catch (e) {
    Vue.toasted.show('Error al cargar el estado de las evaluaciones', {type: 'error'});
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}

export function resetEvaluationState(context) {
  context.commit('resetEvaluationState')
}

export async function setLockState(context, params) {
  var state = context.state
  var evaluationId = state.evaluationId
  try {
    context.commit('setLoading', true, { root: true })
    await Vue.axios({
      method: 'POST',
      url: '/evaluation/setLockState',
      data: {
        evaluationId: evaluationId,
        directorId: params.directorId,
        teacherId: params.teacherId,
        state: params.state
      }
    })
  } catch (e) {
    Vue.toasted.show('Error al realizar la peticion', {type: 'error'});
    console.log(e.message)
  } finally {
    context.commit('setLoading', false, { root: true })
  }

  // Reaload
  await getEvaluationState(context, {
    EvaluationId: evaluationId
  })
}