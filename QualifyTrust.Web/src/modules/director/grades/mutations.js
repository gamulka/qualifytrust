export function setEvaluationState(state, params) {
  state.evaluationId = params.evaluationId
  state.evaluationStates = params.data
}

export function resetEvaluationState(state) {
  state.evaluationStates = []
}