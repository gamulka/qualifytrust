import Vue from 'vue'
import EventBus from '@/plugins/event-bus.js'

export async function setLockState(context, params) {

  try {
    context.commit('setLoading', true, { root: true })
    await Vue.axios({
      method: 'POST',
      url: '/evaluation/setLockState',
      data: {
        evaluationId: params.evaluationId,
        directorId: params.directorId,
        teacherId: params.teacherId,
        state: params.state = params.state
      }
    }).then(function (response) {
      EventBus.$emit('refreshGradesStates', params);
      var message = params.state === 2
        ? 'La evaluación se ha cerrado correctamente!'
        : 'La evaluación del profesor seleccionado se ha abierto correctamente!'

      context.commit('setSuccess', message, { root: true })
    })
  } catch (e) {
    console.log(e.message)
    var message = params.state === 2
      ? 'Error al cerrar la evaluación'
      : 'Error al cerrar la evaluación sel profesor seleccionado'
      
    context.commit('setError', message, { root: true })
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}