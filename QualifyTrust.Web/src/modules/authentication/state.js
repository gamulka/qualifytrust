export default {
  jwt: localStorage.getItem('authToken'),
  user: {
    id: '',
    name: '',
    surName: '',
    email: '',
    role: 0,
    picture: '',
    ethereumAddress: ''
  },
  isLogged: false,
  error: false,
  errorMessage: ''
}