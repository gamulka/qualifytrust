import Vue from 'vue'
import { logout } from './mutations';

export async function signIn(context, user) {
  try {
    context.commit('setLoading', true, { root: true })
    await Vue.axios({
      method: 'POST',
      url: '/auth/login',
      data: user
    })
      .then(function (response) {
        context.commit('setAuthData', response)
      })
  } catch (e) {
    console.log(e.message)
    context.commit('authError', 'Error de autenticación')
  } finally {
    context.commit('setLoading', false, { root: true })
  }
}