import jwt_decode from 'jwt-decode'

const tokenName = 'authToken'

export function setAuthData(state, response) {
  setAuth(state, response)
  setUser(state, response)
}

export function setAuth(state, response) {
  var token = response.data.access_token

  state.jwt = token
  state.isLogged = true
  state.error = false
  state.errorMessage = ''
  localStorage.setItem(tokenName, token)
}

export function setUser(state, response) {
  var token = response.data.access_token
  var decoded = jwt_decode(token)

  state.user.id = decoded.id
  state.user.name = decoded.unique_name
  state.user.surName = decoded.family_name
  state.user.email = decoded.email
  state.user.role = decoded.role
  state.user.picture = decoded.picture
  state.user.ethereumAddress = decoded.ethereumAddress
}

export function cleanAuth(state) {
  state.jwt = null
  state.isLogged = false
}

export function cleanUser(state) {
  state.user.id = ''
  state.user.name = ''
  state.user.surName = ''
  state.user.email = ''
  state.user.role = ''
  state.user.picture = ''
  state.user.ethereumAddress = ''
}
export function logout(state, rootState) {
  cleanAuth(state)
  cleanUser(state)
  localStorage.removeItem(tokenName)
  localStorage.clear()
}

export function authError(state, error) {
  state.error = true
  state.errorMessage = error
}