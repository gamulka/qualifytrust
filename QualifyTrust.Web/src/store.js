import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/modules/authentication'
import evaluationList from '@/modules/evaluationList'
import studentsList from '@/modules/student/studentList'
import directorGrades from '@/modules/director/grades'
import directorLock from '@/modules/director/lock'
import teacherGrades from '@/modules/teacher/grades'
import teacherSign from '@/modules/teacher/sign'
import studentGrades from '@/modules/student/grades'
import VuexPersistence from 'vuex-persist'
Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  modules: [
    'auth',
    'evaluationList'
  ]
})

export default new Vuex.Store({
  state: {
    loading: false,
    error: false,
    success: false,
    message: ''
  },
  mutations: {
    setLoading (state, bool) {
      state.loading = bool
    },
    setSuccess (state, message) {
      state.message = message
      state.success = true
      state.error = false
    },
    setError (state, message) {
      state.message = message
      state.success = false
      state.error = true
    },
    cleanAlert (state) {
      state.success = false
      state.error = false
    }
  },
  modules: {
    auth,
    evaluationList,
    studentsList,
    directorGrades,
    directorLock,
    teacherGrades,
    teacherSign,
    studentGrades
  },
  plugins: [vuexLocal.plugin]
})
