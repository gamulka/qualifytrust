import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'
import store from './store'
Vue.use(Router)

const router = new Router({
  routes
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const requiresRole = to.matched.some(record => record.meta.requiresRole)
  const isValidRole = to.matched.some(record => record.meta.role == store.state.auth.user.role)
  const isLogged = store.state.auth.isLogged

  if ( ! requiresAuth && isLogged && to.path === '/login') {
    return next('/home')
  }

  if( (requiresAuth && requiresRole) && ! isValidRole ) {
    return next('/login')
  }

  if ( requiresAuth && ! isLogged ) {
    next('/login')
  } else {
    next()
  }
})

export default router
