pragma solidity >= 0.4.24;

contract StudentContract {
    
    address public owner;
    
    struct StudentData {
        address addr;
        string id;
        string name;
    }

    StudentData public Student;
    
    mapping(address => bool) Directors;
    mapping(address => bool) Teachers;

    constructor() public {
        owner = msg.sender;
    }
    
    modifier isOwner() {
        require(msg.sender == owner);
        _;
    }

    function SetStudentData (address addr, string id, string name) public isOwner() {
        Student.addr = addr;
        Student.id = id;
        Student.name = name;
    }
    
    function SetDirector (address directorAddress) public isOwner() {
        Directors[directorAddress] = true;
    }
    
    function SetTeacher (address teacherAddress) public isOwner() {
        Teachers[teacherAddress] = true;
    }
    
}