# QualifyTrust

School grades over blockchain

**Requisitos/Instrucciones de inicio:**

* En el fichero appSettings.config modificar la key *QualifyTrust.Daemon:ConnectionString* para que el Data Source haga referencia a un directorio que exista, actualmente se usa C:\tmp\ (el nombre de la db 'QualifyTrust.sqlite' se puede quedar igual )
* Ejecutar la soluci�n con el proyecto de inicio QualifyTrust.Daemon.
* Indicar en el navegador la direcci�n donde escucha el daemon (http://localhost:2380).

**RoadMap:**

* [X] Commit inicial con el WebApi en QualifyTrust.Daemon
* [X] Eliminar el repo de MongoDb y usar SQLite
* [.] Aplicar modificaiones iniciales sobre los datos iniciales cargados (stubs) para crear el contexto de prueba(ej.: Poner como firmadas todas las evaluaciones salvo la de uno o dos profesores)
* [X] Implementar fachada Ethereum para usar blockchain
* [ ] Implementar el look and feell.
* [ ] ...
